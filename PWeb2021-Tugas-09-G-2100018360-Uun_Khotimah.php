<?php
	$gaji = 1000000;
	$pajak = 0.1;
	$donasi = 50000;
	$hitung = $gaji - ($gaji*$pajak);
	$thp = $hitung - $donasi;

    echo "Listing Program 9.1 <br>";
    echo "Total gaji Anda bulan ini <br>";
	echo "Sebelum pajak = Rp. $gaji <br>";
	echo "Setelah pajak = Rp. $hitung <br>";
	echo "Gaji yang dibawa pulang = Rp. $thp";
	echo "<br>-------------------------------------------------<br>";
	echo "<br><br>";

    $a = 5;
    $b = 4;
    $c = "4";

    echo "Listing Program 9.2 <br>";
    echo "Comparison Operator <br>";

    echo "($a == $b) : " . ($a == $b);
    echo "<br /> ($b == $c) :" . ($b == $c);
    
    echo "<br /> ($a != $b):" . ($a != $b);
    echo "<br /> ($b !== $c):" .  ($b !== $c);

    echo "<br /> ($a > $b) :" . ($a > $b);
    echo "<br /> ($b > $a) :" . ($b > $a);

    echo "<br /> ($a < $b) :" . ($a < $b);
    echo "<br /> ($b < $a) :" . ($b < $a);

    echo "<br /> ($a <= $b) :" . ($a <= $b);
    echo "<br /> ($b <= $c) :" . ($b <= $c);
    echo "<br /> ($b <= $a) :" . ($b <= $a);

    echo "<br> ($a == $b) && ($a > $b) : ".(($a != $b) && ($a > $b));
	echo "<br> ($a || $b) && ($a > $b) : ".(($a != $b) || ($a > $b));
?>