<style>
	body{
		text-align: center;
		margin: 300px;
		font-family: vidaloka;
		font-size: 23px;
		background: linear-gradient(to top, #7B68EE 0%, #F0E68C 100%); 
	}
</style>
<?php
//fungsi ini tanpa return value, & tanpa parameter
function cetak_ganjil(){
	for($i = 0; $i < 100; $i++){
		if($i%2==1){
			echo "$i, ";
		}
	}
}
//Pemanggilan fungsi
cetak_ganjil();
?>