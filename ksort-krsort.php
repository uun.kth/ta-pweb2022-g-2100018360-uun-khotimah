<style>
	body{
		text-align: center;
		margin: 300px;
		font-family: vidaloka;
		font-size: 17px;
		background: linear-gradient(to top, #FFEBCD 0%, #F0E68C 100%); 
	}
</style>
<?php
$arrNilai=array("Fulan" => 80, "Fulin" => 90, "Fulun"=>75, "Falan"=>85);
echo "<b>Array sebelum diurutkan</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

asort($arrNilai);
reset($arrNilai);
echo "<b>Array setelah diurutkan dengan ksort()</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

arsort($arrNilai);
reset($arrNilai);
echo "<b>Array setelah diurutkan dengan krsort()</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";
?>