<style>
	body{
		text-align: center;
		margin: 300px;
		font-family: vidaloka;
		font-size: 23px;
		background: linear-gradient(to top, #7B68EE 0%, #F0E68C 100%); 
	}
</style>
<?php
// fungsi ini dengan return value, & parameter
function cetak_ganjil($awal, $akhir){
	for ($i = $awal; $i < $akhir; $i++){
		if ($i%2==1){
			echo "$i, ";
		}
	}
} 
// pemanggilan fungsi 
$a = 5;
$b = 50;
echo "<b> Bilangan ganjil dari $a sampai $b, adalah : </b><br>";
cetak_ganjil($a,$b);
?>