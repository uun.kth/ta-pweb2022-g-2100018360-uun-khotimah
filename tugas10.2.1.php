<html>
<head>
	<title>Tugas 10.2.1</title>
</head>
<style>
	div{
		text-align: center;
		background: linear-gradient(to bottom, #66ccff 0%, #ff66cc 100%);
		height: 700px;
	}
</style>
<body>
	<div>
		<form method="post">
			<label><h2>Cek Nilai Anda</h2></label>
			<input type="number" name="nilai" placeholder="masukkan nilai">
			<input type="submit" name="submit" value="proses">
		</form><br>

		<?php
		    if (isset($_POST['submit'])){
		    	$nilai = $_POST['nilai'];
		    } if($nilai >= 80.00) {
		    	echo "Nilai Anda $nilai, Anda mendapat A";
		    } elseif ($nilai >= 76.25) {
		    	echo "Nilai Anda $nilai, Anda mendapat A-";
		    } elseif ($nilai >= 68.75) {
		    	echo "Nilai Anda $nilai, Anda mendapat B+";
		    } elseif ($nilai >= 65.00) {
		    	echo "Nilai Anda $nilai, Anda mendapat B";
		    } elseif ($nilai >= 62.50) {
		    	echo "Nilai Anda $nilai, Anda mendapat B-";
		    } elseif ($nilai >= 57.50) {
		    	echo "Nilai Anda $nilai, Anda mendapat C+";
		    } elseif ($nilai >= 55.00) {
		    	echo "Nilai Anda $nilai, Anda mendapat C";
		    } elseif ($nilai >= 51.25) {
		    	echo "Nilai Anda $nilai, Anda mendapat C-";
		    } elseif ($nilai >= 43.75) {
		    	echo "Nilai Anda $nilai, Anda mendapat D+";
		    } elseif ($nilai >= 40.00) {
		    	echo "Nilai Anda $nilai, Anda mendapat D";
		    } else {
		    	echo "Nilai Anda $nilai, Anda mendapat E";
		    }
		?>
	</div>
</body>
</html>