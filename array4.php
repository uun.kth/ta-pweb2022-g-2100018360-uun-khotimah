<style>
	body{
		text-align: center;
		margin: 300px;
		font-family: vidaloka;
		font-size: 23px;
		background: linear-gradient(to top, #FFEBCD 0%, #F0E68C 100%); 
	}
</style>
<body>
<?php
	$arrNilai=array("Fulan"=> 80, "Fulin"=>90, "Fulun"=>75, "Falan"=>85);
	echo "Menampilkan isi array asosiatif dengan foreach: <br>";
	foreach ($arrNilai as $nama=>$nilai) {
		echo "Nilai $nama=$nilai <br>";
	}

	reset($arrNilai);
	echo "Menampilkan isi array asosiatif dengan WHILE dan LIST: <br>";
	foreach ($arrNilai as $nama=>$nilai) {
		echo "Nilai $nama=$nilai <br>";
	}
?>
</body>