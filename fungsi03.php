<style>
	body{
		text-align: center;
		margin: 300px;
		font-family: vidaloka;
		font-size: 23px;
		background: linear-gradient(to top, #7B68EE 0%, #F0E68C 100%); 
	}
</style>
<?php
//Fungsi dengan return value dan parameter
function luas_lingkaran($jari){
	return 3.14*$jari*$jari;
}

// pemanggilan fungsi
$r=20;
echo "Luas Lingkaran dengan jari-jari $r = ";
echo luas_lingkaran($r);
?>